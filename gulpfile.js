'use strict';
/*jshint esversion: 6 */
/*jshint node: true */

const gulp = require('gulp');
const source = require('vinyl-source-stream');
const through = require('through');
const browserify = require('browserify');
const jade = require('jade');

gulp.task('js', () => {

	const b = browserify('src/index.js');

	b.transform(file => {

		if (!/\.jade$/i.test(file)) {
			return through();
		}

		let data = '';
		return through(
			buf => { data += buf.toString(); },
			function () {
				try {
					this.queue(`
						module.exports = require('jade').compile(
							${JSON.stringify(data)},
							{ pretty: true }
						);
					`);
				} catch (e) {
					this.emit('error', e);
				}
				this.queue(null);
			}
		);
	});

	return b.bundle()
		.pipe(source('bundle.js'))
		.pipe(gulp.dest('dist'));
});

gulp.task('default', ['js']);
